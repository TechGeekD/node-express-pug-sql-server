"user strict";
const dotenv = require("dotenv").config();

if (dotenv.error) {
  throw new Error(dotenv.error);
}

var path = require("path");
var logger = require("morgan");
var express = require("express");
const helmet = require("helmet");
var createError = require("http-errors");
const compression = require('compression')
var cookieParser = require("cookie-parser");
const expressSanitizer = require("express-sanitizer");

var indexRouter = require("./routes/index");
var symbolRouter = require("./routes/symbol");

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(helmet());
app.use(expressSanitizer());
app.use(compression()); // compress all responses

app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/symbol", symbolRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
