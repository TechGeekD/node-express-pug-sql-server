'use strict';
module.exports = (sequelize, DataTypes) => {
  var symbol = sequelize.define('symbol', {
    market: DataTypes.STRING,
    symbol: DataTypes.STRING,
    name: DataTypes.STRING,
    parent_symbol_id: DataTypes.STRING,
    split_date: DataTypes.TIME
  }, {});
  symbol.associate = function(models) {
    // associations can be defined here
  };
  return symbol;
};