var express = require("express");
var router = express.Router();
var model = require("../models/index");

//middleware to hanlde errors
const awaitErorrHandlerFactory = middleware => {
  return async (req, res, next) => {
    try {
      await middleware(req, res, next);
    } catch (err) {
      next(err);
    }
  };
};

/* GET symbol listing. */
router.get(
  "/",
  awaitErorrHandlerFactory(async (req, res, next) => {
    // const symbol = await model.symbol.findAll({});
    model.sequelize
      .query(
        `SELECT *
    FROM [inspiron].[dbo].[history] h
    join [nextdb].[dbo].[symbol] s
    on s.id=h.symbol
    where s.Market='BSE' 
    and date='2018-04-02 00:00:00.0000000'`
      )
      .then(function(symbol) {
        console.log("success");
        return res.json({
          error: false,
          data: symbol
        });
      });
  })
);

/* POST symbol. */
router.post(
  "/",
  awaitErorrHandlerFactory(async (req, res, next) => {
    const { market, symbol, name, parent_symbol_id, split_date } = req.body;
    const _symbol = model.symbol.create({
      market: market,
      symbol: symbol,
      name: name,
      parent_symbol_id: parent_symbol_id,
      split_date: split_date
    });
    return res.status(201).json({
      error: false,
      data: _symbol,
      message: "New symbol has been created."
    });
  })
);

/* update symbol. */
router.put(
  "/:id",
  awaitErorrHandlerFactory(async (req, res, next) => {
    const symbol_id = req.params.id;

    const { market, symbol } = req.body;

    model.symbol.update(
      { market: market, symbol: symbol },
      { where: { id: symbol_id } }
    );

    return res.status(201).json({
      error: false,
      message: "symbol has been updated."
    });
  })
);

/* GET symbol listing. */
router.delete(
  "/:id",
  awaitErorrHandlerFactory(async (req, res, next) => {
    const symbol_id = req.params.id;

    model.symbol.destroy({
      where: {
        id: symbol_id
      }
    });

    return res.status(201).json({
      error: false,
      message: "symbol has been delete."
    });
  })
);

module.exports = router;
